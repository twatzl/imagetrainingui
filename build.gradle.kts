import org.jetbrains.compose.compose
import org.jetbrains.compose.desktop.application.dsl.TargetFormat
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.30"
    id("org.jetbrains.compose") version "0.2.0-build132"
    id("org.bytedeco.gradle-javacpp-platform") version "1.5.4"
}

group = "eu.twatzl"
version = "1.0.0"

repositories {
    google()
    mavenCentral()
    maven { url = uri("https://maven.pkg.jetbrains.space/public/p/compose/dev") }
    maven(url = "https://kotlin.bintray.com/kotlinx")
    maven(url = "https://dl.bintray.com/kodein-framework/Kodein-DB")
}

buildscript {
    repositories {
        google()
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
    }

    dependencies {
        classpath("org.jetbrains.compose:compose-gradle-plugin:0.3.1")
        classpath("com.android.tools.build:gradle:4.0.1")
    }
}

dependencies {
    implementation(compose.desktop.currentOs)

    implementation("org.kodein.db:kodein-db-jvm:0.4.0-beta")
    implementation("org.kodein.db:kodein-db-serializer-kryo-jvm:0.4.0-beta") // use kotlinx for multiplatform
    implementation("org.kodein.db:kodein-leveldb-jni-jvm:0.4.0-beta") // -linux, -macos or -windows*/

    //implementation(fileTree(baseDir = "libs", "*.jar"]))
    //implementation("org.bytedeco.javacv:1.3.1")
    implementation("org.bytedeco:opencv-platform:4.5.1-1.5.5")
    implementation("android.arch.lifecycle:livedata:1.1.1")

    testImplementation(kotlin("test-junit5"))
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.6.0")
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "11"
}

compose.desktop {
    application {
        mainClass = "MainKt"
        nativeDistributions {
            targetFormats(TargetFormat.Dmg, TargetFormat.Msi, TargetFormat.Deb)
            packageName = "ImageTrainingUI"
        }
    }
}
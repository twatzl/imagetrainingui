import util.ImageUtil.loadImageThumbnail
import java.nio.file.Path

/**
 * A training image which has been preprocessed already. i.e. cropping has been applied.
 * One TrainingImage may result in multiple preprocessed training images if operations
 * such as cropping random parts or random orientation are applied.
 */
class PreprocessedTraningImage (
    val id: String,
    val trainingImageId: String,
    val path: Path,
) {
    fun thumbnail(scaleShortestSide: Int = -1) = loadImageThumbnail(path, scaleShortestSide)
}
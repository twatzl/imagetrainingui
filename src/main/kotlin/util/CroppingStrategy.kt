package util

import org.bytedeco.opencv.opencv_core.Mat

interface CroppingStrategy {

    val name: String

    fun cropImage(img: Mat, width: Int, height: Int): Mat

}
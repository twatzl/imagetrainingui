package util

data class Dimensions(
    val width: Int,
    val height: Int,
)

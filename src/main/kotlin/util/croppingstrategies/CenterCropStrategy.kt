package util.croppingstrategies

import org.bytedeco.opencv.opencv_core.Mat
import util.CroppingStrategy
import util.ImageUtil

class CenterCropStrategy: CroppingStrategy {

    override val name = "centerCrop"

    override fun cropImage(img: Mat, width: Int, height: Int): Mat = ImageUtil.cropInCenter(img, width, height)
}
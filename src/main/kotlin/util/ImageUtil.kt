package util

import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap
import org.bytedeco.opencv.opencv_core.Mat
import org.bytedeco.opencv.opencv_core.Rect
import org.jetbrains.skija.Image
import java.awt.geom.AffineTransform
import java.awt.image.AffineTransformOp
import java.awt.image.BufferedImage
import java.io.ByteArrayOutputStream
import java.nio.file.Path
import javax.imageio.ImageIO
import kotlin.math.floor
import kotlin.math.roundToInt
import java.awt.Dimension
import java.io.File
import javax.imageio.ImageReader

import javax.imageio.stream.ImageInputStream




object ImageUtil {

    fun cropInCenter(img: Mat, width: Int, height: Int): Mat {
        require(img.cols() > width) // TODO: think about meaningful error message
        require(img.rows() > height) // TODO: think about meaningful error message

        val centerX = img.cols() / 2
        val centerY = img.rows() / 2

        val x = centerX - floor(width / 2.0).toInt()
        val y = centerY - floor(height / 2.0).toInt()

        return Mat(img, Rect(x, y, width, height))
    }

    // TODO: move to common tools lib
    fun loadImageThumbnail(imagePath: Path, scaleShortestSide: Int = -1): ImageBitmap {
        // TODO: for now we assume we only load .jpg files.
        // we should also support raw files here

        val file = imagePath.toFile()
        requireNotNull(file) { "Resource $imagePath not found" } // TODO: fix error
        val image = file.inputStream().use(ImageIO::read)

        val scaledImage = if (scaleShortestSide > 0) {
            val aspect = image.width / image.height.toFloat()
            val newWidth: Int
            val newHeight: Int
            if (image.width > image.height) {
                // height is short side
                newWidth = (aspect * scaleShortestSide).roundToInt()
                newHeight = scaleShortestSide
            } else {
                newHeight = (scaleShortestSide / aspect).roundToInt()
                newWidth = scaleShortestSide
            }
            scaleImage(image, newWidth, newHeight, AffineTransformOp.TYPE_BILINEAR)
        } else {
            image
        }

        return scaledImage.toImageBitmap()
    }

    // TODO: move to common tools lib
    fun scaleImage(image: BufferedImage, targetWidth: Int, targetHeight: Int, transformType: Int): BufferedImage {
        val actualWidth = image.width
        val actualHeight = image.height
        val horizontalScale = if (targetWidth > actualWidth) {
            actualWidth / targetWidth.toDouble()
        } else {
            targetWidth.toDouble() / actualWidth
        }
        val verticalScale = if (targetHeight > actualHeight) {
            actualHeight / targetHeight.toDouble()
        } else {
            targetHeight.toDouble() / actualHeight
        }
        var after = BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_INT_ARGB)
        val at = AffineTransform()
        at.scale(horizontalScale, verticalScale)
        val scaleOp = AffineTransformOp(at, transformType)
        val result = scaleOp.filter(image, after)
        return result
    }

    // TODO: move to common tools lib
    fun BufferedImage.toImageBitmap(): ImageBitmap = Image.makeFromEncoded(
        toByteArray(this)
    ).asImageBitmap()

    // TODO: move to common tools lib
    fun toByteArray(bitmap: BufferedImage) : ByteArray {
        val baos = ByteArrayOutputStream()
        ImageIO.write(bitmap, "png", baos)
        return baos.toByteArray()
    }

    fun getImageDimensions(image: File): Dimensions {
        ImageIO.createImageInputStream(image).use { img ->
            val readers: Iterator<ImageReader> = ImageIO.getImageReaders(img)
            require (readers.hasNext()) {
                "cannot read image dimensions for file: ${image.absolutePath}"
            }

            val reader: ImageReader = readers.next()
            return try {
                reader.input = img
                Dimensions(reader.getWidth(0), reader.getHeight(0))
            } finally {
                reader.dispose()
            }
        }
    }
}
data class TrainingImagePair(
    val imageA: PreprocessedTraningImage,
    val imageB: PreprocessedTraningImage,
) {

    fun isEquivalent(other: TrainingImagePair): Boolean {
        return other.imageB == imageA && other.imageA == imageB
    }



}

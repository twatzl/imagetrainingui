import androidx.compose.desktop.Window
import androidx.compose.material.Text
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.desktop.LocalAppWindow
import androidx.compose.foundation.layout.Column
import ranking.impl.SharpnessRankingController
import ui.rankingWindow
import java.io.File
import java.io.FilenameFilter
import java.nio.file.Path
import javax.swing.JFileChooser
import javax.swing.UIManager
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.Path
import kotlin.io.path.listDirectoryEntries


@ExperimentalPathApi
fun main() = Window {

    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName()) // avoid that ugly JFileChooser look

    var text by remember { mutableStateOf("Hello, World!") }
    var images by remember { mutableStateOf(listOf<Path>()) }
    var selectedFolder by remember { mutableStateOf<File?>(null)}

    MaterialTheme {

        Column {
            Text("selected folder: ${selectedFolder?.absolutePath ?: "none"}")
            Text("${images.size} images selected")
            Button(onClick = {
            }) {
                Text("Button")
            }
            Button(onClick = {}) {
                Text("Choose Database")
            }
            Button(onClick = {}) {
                Text("Choose Tmp Folder")
            }
            Button(onClick = {
                var selectedFolder: File?
                JFileChooser(System.getProperty("user.home")).apply {
                    fileSelectionMode = JFileChooser.DIRECTORIES_ONLY
                    showOpenDialog(null)
                    selectedFolder = selectedFile
                } // ...more stuff
                selectedFolder?.let { folder ->
                    images = loadImagesInFolder(folder)
                }

            }) {

                Text("Load Folder")
            }
            Button(onClick = {}) {
                Text("Load Folder Recursive")
            }
            Button(onClick = {
                Window{
                    val rankingController = SharpnessRankingController(Path("tmp"), images)
                    rankingWindow(rankingController)
                }

            }) {
                Text("Start Training")
            }
        }
    }
}

@ExperimentalPathApi
private fun loadImagesInFolder(folder: File) = folder.list { _, name ->
    name.toLowerCase().endsWith(".jpg") || name.toLowerCase().endsWith(".jpeg")
}?.map { filename ->
    folder.toPath().resolve(Path(filename))
} ?: emptyList()
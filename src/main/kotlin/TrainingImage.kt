import org.kodein.memory.util.UUID
import java.nio.file.Path

open class TrainingImage(
    val id: String = UUID.randomUUID().toString(),
    val imageHash: String = "", // TODO different datatype + calculate
    val originalFilename: String,
    val originalSize: Long,
    val path: Path?,
) {
    // TODO: equals + hashcode
}

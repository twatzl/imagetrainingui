package ranking

import TrainingImagePair
import java.io.File
import java.nio.file.Path

interface RankingController {

    /**
     * The question which should be displayed at the top
     */
    val question: String

    /**
     * List of actions that can be performed for each image
     */
    val actions: List<Pair<String, () -> Unit>>

    val trainingImagePair: TrainingImagePair

    fun registerImageForTraining(image: Path)

    fun registerImagesForTraining(images: List<Path>) {
        images.forEach { registerImageForTraining(it) }
    }

    fun persistToDatabase()

    fun loadFromDatabase()

    fun exportData(target: File) // TODO: export format?

}
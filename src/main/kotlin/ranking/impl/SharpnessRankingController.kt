package ranking.impl

import PreprocessedTraningImage
import TrainingImage
import TrainingImagePair
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import org.bytedeco.opencv.global.opencv_imgcodecs.imread
import org.bytedeco.opencv.global.opencv_imgcodecs.imwrite
import ranking.RankingController
import ranking.model.PairRankingResult
import ranking.model.TrainingImagePairRanking
import util.CroppingStrategy
import util.ImageUtil.getImageDimensions
import util.croppingstrategies.CenterCropStrategy
import java.io.File
import java.nio.file.Path
import java.util.*
import javax.imageio.ImageIO
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.absolutePathString

@ExperimentalPathApi
class SharpnessRankingController(
    private val preprocessedImageDir: Path,
    private val initialTrainingImages: List<Path>,
) : RankingController {

    override val question: String
        get() = "Which image is sharper?"
    override val actions: List<Pair<String, () -> Unit>>
        get() = listOf(
            "Left" to { leftClicked() },

            "No difference" to { equalClicked() },

            "Not sure" to { skipClicked() },

            "Right" to { rightClicked() },
        )

    override val trainingImagePair: TrainingImagePair
        get() = currentTrainingImagePair

    private lateinit var currentTrainingImagePair: TrainingImagePair

    // raw images stored on file system
    private val registeredTrainingImages = mutableSetOf<TrainingImage>()

    // cropped images
    private val preprocessedTrainingImages = mutableSetOf<PreprocessedTraningImage>()

    private val shownTrainingImagePairs = mutableMapOf<String, MutableList<String>>()

    private val results = mutableListOf<TrainingImagePairRanking>()

    private val preprocessingStrategies = listOf<CroppingStrategy>(
        CenterCropStrategy()
    )

    init {
        require(initialTrainingImages.size > 2)
        registerImagesForTraining(initialTrainingImages)
        generateNextTrainingImagePair()

        preprocessedImageDir.toFile().mkdirs()
    }

    private fun leftClicked() {
        storeResult(PairRankingResult.AIsBetter)
        generateNextTrainingImagePair()
    }

    private fun equalClicked() {
        storeResult(PairRankingResult.Equal)
        generateNextTrainingImagePair()
    }

    private fun skipClicked() {
        // do nothing
        generateNextTrainingImagePair()
    }

    private fun rightClicked() {
        storeResult(PairRankingResult.BIsBetter)
        generateNextTrainingImagePair()
    }

    private fun storeResult(result: PairRankingResult) {
        val trainingImagePair = currentTrainingImagePair
        requireNotNull(trainingImagePair)
        val result = TrainingImagePairRanking(
            imageAId = trainingImagePair.imageA.id,
            imageBId = trainingImagePair.imageB.id,
            result = result.code,
        )
        results.add(result)
    }

    private fun generateNextTrainingImagePair() {
        val availableImages = this.registeredTrainingImages.flatMap { trainingImage ->
            preprocessingStrategies.map { strategy ->
                Pair(
                    trainingImage,
                    getPreprocessedImageId(trainingImage, strategy)
                )
            }
        }.associate { it.second to it.first }
        val availableImageIds = availableImages.keys

        lateinit var image1Id: String
        var image2Id: String? = null
        while (image2Id == null) {
            image1Id = availableImageIds.random()
            val shownImages = shownTrainingImagePairs[image1Id] ?: emptySet()
            val remainingImages = availableImageIds.minus(shownImages.toSet()).minus(image1Id)
            if (remainingImages.isEmpty()) {
                continue
            }
            image2Id = remainingImages.random()
        }

        preprocessTrainingImage(availableImages[image1Id]!!, preprocessingStrategies[0])
        preprocessTrainingImage(availableImages[image2Id]!!, preprocessingStrategies[0])

        val image1 = preprocessedTrainingImages.find { it.id == image1Id }
        val image2 = preprocessedTrainingImages.find { it.id == image2Id }

        requireNotNull(image1)
        requireNotNull(image2)

        this.currentTrainingImagePair = TrainingImagePair(
            image1,
            image2
        )
    }

    private fun preprocessTrainingImage(image: TrainingImage, croppingStrategy: CroppingStrategy) {
        val imagePath = image.path?.absolutePathString()
        requireNotNull(imagePath)
        if (preprocessedTrainingImages.find { it.id == getPreprocessedImageId(image, croppingStrategy) } != null) {
            // image already was preprocessed
            return
        }

        val img = imread(imagePath)
        val preprocessedImageId = getPreprocessedImageId(image, croppingStrategy)
        val targetFileName = "$preprocessedImageId.jpg"
        val destPath = preprocessedImageDir.resolve(targetFileName)
        val result = croppingStrategy.cropImage(img, 224, 224) // TODO: use consts

        imwrite(destPath.absolutePathString(), result)
        preprocessedTrainingImages.add(
            PreprocessedTraningImage(
                preprocessedImageId,
                image.id,
                destPath,
            )
        )
    }

    private fun getPreprocessedImageId(image: TrainingImage, croppingStrategy: CroppingStrategy) =
        "${image.id}_${croppingStrategy.name}"

    override fun registerImageForTraining(image: Path) {
        val fileName = image.fileName
        requireNotNull(fileName)

        val imageDimensions = getImageDimensions(image.toFile())
        if (imageDimensions.height < SelectionHeight || imageDimensions.width < SelectionWidth) {
            // do not register images which are too small
            return
        }

        val trainingImage = TrainingImage(
            id = UUID.randomUUID().toString(),
            imageHash = "", // TODO
            originalFilename = fileName.toString(),
            originalSize = image.toFile().length(),
            path = image,
        )

        this.registeredTrainingImages.add(trainingImage)
    }

    override fun persistToDatabase() {
        TODO("Not yet implemented")
    }

    override fun loadFromDatabase() {
        TODO("Not yet implemented")
    }

    override fun exportData(target: File) {
        TODO("Not yet implemented")
    }

    companion object {
        const val SelectionWidth = 224
        const val SelectionHeight = 224
    }
}
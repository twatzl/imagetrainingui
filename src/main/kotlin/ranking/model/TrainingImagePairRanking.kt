package ranking.model

import java.util.*

/**
 * @param id
 * @param imagePair
 * @param result If result < 0 then imageA has lower score than image, if result > 0 imageA has higher score than imageB
 *  and if result == 0 both images have the same score.
 */
data class TrainingImagePairRanking(
    val id: String = UUID.randomUUID().toString(),
    val imageAId: String,
    val imageBId: String,
    val result: Int,
)

enum class PairRankingResult(val code: Int) {
    AIsBetter( 1),
    BIsBetter(-1),
    Equal(0)
}
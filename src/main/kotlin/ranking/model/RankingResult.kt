package ranking.model

data class RankingResult(
    // a map that maps image id to result score
    val result: Map<String, Double>
)

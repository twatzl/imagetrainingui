package ui

import TrainingImagePair
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import ranking.RankingController

@Composable
fun rankingWindow(rankingTrainingController: RankingController) {

    var imagePair: TrainingImagePair by remember { mutableStateOf(rankingTrainingController.trainingImagePair) }

    Text(rankingTrainingController.question)
    Column {
        Row {
            Text(rankingTrainingController.question)
        }
        Row {
            Image(imagePair.imageA.thumbnail(), "The first image")
            Image(imagePair.imageB.thumbnail(), "The second image")
        }
        Row {
            rankingTrainingController.actions.forEach { action ->
                Button(onClick = {
                    action.second()
                    imagePair = rankingTrainingController.trainingImagePair
                }
                ) {
                    Text(action.first)
                }
            }
        }
    }
}